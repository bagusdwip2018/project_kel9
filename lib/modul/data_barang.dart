class Data_barang {
  String? kode_barang;
  String? nama_barang;
  String? jumlah;

  Data_barang({
    this.kode_barang,
    this.nama_barang,
    this.jumlah,
  });

  factory Data_barang.fromJson(Map<String, dynamic> json) => Data_barang(
        kode_barang: json['kode_barang'],
        nama_barang: json['nama_barang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kode_barang': this.kode_barang,
        'nama_barang': this.nama_barang,
        'jumlah': this.jumlah,
      };
}
