class Data_pengembalian_detail{
  String? mhsNpm;
  String? mhsNama;
  String? mhsAlamat;
  String? mhsFakultas;
  String? mhsProdi;

  Data_pengembalian_detail({
    this.mhsNpm,
    this.mhsNama,
    this.mhsAlamat,
    this.mhsFakultas,
    this.mhsProdi,
  });

  factory Data_pengembalian_detail.fromJson(Map<String, dynamic> json) => Data_pengembalian_detail(
        mhsNpm: json['mhsNpm'],
        mhsNama: json['mhsNama'],
        mhsAlamat: json['mhsAlamat'],
        mhsFakultas: json['mhsFakultas'],
        mhsProdi: json['mhsProdi'],
      );

  Map<String, dynamic> toJson() => {
        'mhsNpm': this.mhsNpm,
        'mhsNama': this.mhsNama,
        'mhsAlamat': this.mhsAlamat,
        'mhsFakultas': this.mhsFakultas,
        'mhsProdi': this.mhsProdi,
      };
}
