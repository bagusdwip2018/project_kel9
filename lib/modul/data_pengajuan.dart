class Data_pengajuan {
  String? kode_pengajuan;
  String? tanggal;
  String? npm_peminjam;
  String? nama_peminjam;
  String? prodi;
  String? nomer_handphone;

  Data_pengajuan({
    this.kode_pengajuan,
    this.tanggal,
    this.npm_peminjam,
    this.nama_peminjam,
    this.prodi,
    this.nomer_handphone,
  });

  factory Data_pengajuan.fromJson(Map<String, dynamic> json) => Data_pengajuan(
        kode_pengajuan: json['kode_pengajuan'],
        tanggal: json['tanggal'],
        npm_peminjam: json['npm_peminjam'],
        nama_peminjam: json['nama_peminjam'],
        prodi: json['prodi'],
        nomer_handphone: json['nomer_handphone'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengajuan': this.kode_pengajuan,
        'tanggal': this.tanggal,
        'npm_peminjam': this.npm_peminjam,
        'nama_peminjam': this.nama_peminjam,
        'prodi': this.prodi,
        'nomer_handphone': this.nomer_handphone
      };
}
