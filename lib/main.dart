import 'package:flutter/material.dart';
import 'package:flutter_application_2/Screens/Login/LoginScreens.dart';
import 'package:flutter_application_2/routes.dart';
import 'package:flutter_application_2/theme.dart';
import 'package:flutter_application_2/modul/user.dart';
import 'package:flutter_application_2/Screens/Register/RegistrasiScreens.dart';
import 'package:flutter_application_2/Screens/dasboard/dashboard_admin.dart';
import 'package:flutter_application_2/utils/constants.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

import 'Components/Login/LoginForm.dart';
import 'Screens/Register/RegisterForm.dart';
import 'home/Home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primaryColor: kColorRedSlow,
        scaffoldBackgroundColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(builder: (context, AsyncSnapshot<User?> snapshot) {
        return DashboardAdmin();
      }),
      routes: routes,
    );
  }
}

