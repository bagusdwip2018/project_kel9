// import 'package:flutter/material.dart';
// import 'package:flutter_application_2/modul/data_pengajuan_detail.dart';
// import 'package:flutter_application_2/utils/constants.dart';
// import 'package:get/get.dart';
// import 'package:get/get_navigation/src/dialog/dialog_route.dart';
// import 'package:flutter_application_2/event/event_db.dart';
// import 'package:flutter_application_2/Screens/Register/add_update_data_pengajuan_detail.dart';

// import '../../modul/data_pengajuan_detail.dart';

// class ListData_pengajuan_detail extends StatefulWidget {
//   @override
//   State<ListData_pengajuan_detail> createState() => _ListData_pengajuan_detailState();
// }

// class _ListData_pengajuan_detailState extends State<ListData_pengajuan_detail> {
//   List<Data_pengajuan_detail> _listData_pengajuan_detail = [];
  
//   get data_pengajuan_detail => null;

//   void getData_pengajuan_detail() async {
//     _listData_pengajuan_detail = await EventDb.getData_pengajuan_detail();

//     setState(() {});
//   }

//   @override
//   void initState() {
//     getData_pengajuan_detail();
//     super.initState();
//   }

//   void showOption(Data_pengajuan_detail? user) async {
//     var result = await Get.dialog(
//         SimpleDialog(
//           children: [
//             ListTile(
//               onTap: () => Get.back(result: 'update'),
//               title: Text('Update'),
//             ),
//             ListTile(
//               onTap: () => Get.back(result: 'delete'),
//               title: Text('Delete'),
//             ),
//             ListTile(
//               onTap: () => Get.back(),
//               title: Text('Close'),
//             )
//           ],
//         ),
//         barrierDismissible: false);
//     switch (result) {
//       case 'update':
//         Get.to(add_update_data_pengajuan_detail(Data_pengajuan_detail: data_pengajuan_detail))
//         ?.then((value) => getData_pengajuan_detail());
//         break;
//       case 'delete':
//         EventDb.delete_data_pengajuan_detail(Data_pengajuan_detail!.kode_pengajuan !).then((value) => getData_pengajuan_detail());
//         break;
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: GradientAppBar(
//         gradient: LinearGradient(
//             colors: [kDefaultIconDarkColor, mBlueColor]),
//         // titleSpacing: 0,
//         title: Text('List Data_pengajuan_detail'),
//         // backgroundColor: Asset.colorPrimary,
//       ),
//       body: Stack(
//         children: [
//           _listData_pengajuan_detail.length > 0
//               ? ListView.builder(
//                   itemCount: _listData_pengajuan_detail.length,
//                   itemBuilder: (context, index) {
//                     Data_pengajuan_detail user = _listData_pengajuan_detail[index];
//                     return ListTile(
//                       leading: CircleAvatar(
//                         child: Text('${index + 1}'),
//                         backgroundColor: Colors.white,
//                       ),
//                       title: Text(user.kode_pengajuan?? ''),
//                       subtitle: Text(user.kode_barang ?? ''),
//                       trailing: IconButton(
//                           onPressed: () => showOption(user),
//                           icon: Icon(Icons.more_vert)),
//                     );
//                   },
//                 )
//               : Center(
//                   child: Text("Data Kosong"),
//                 ),
//           Positioned(
//             bottom: 16,
//             right: 16,
//             child: FloatingActionButton(
//               onPressed: () =>
//                   Get.to(AddUpdateData_pengajuan_detail())?.then((value) => getData_pengajuan_detail()),
//               child: Icon(Icons.add),
//               backgroundColor:kColorRedSlow,
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

// class GradientAppBar extends StatelessWidget implements PreferredSizeWidget {
//   static const _defaultHeight = 56.0;

//   final double elevation;
//   final Gradient gradient;
//   final Widget title;
//   final double barHeight;

//   GradientAppBar(
//       {this.elevation = 3.0,
//       required this.gradient,
//       required this.title,
//       this.barHeight = _defaultHeight});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 80.0,
//       decoration: BoxDecoration(gradient: gradient, boxShadow: [
//         BoxShadow(
//           offset: Offset(0, elevation),
//           color: Colors.black.withOpacity(0.3),
//           blurRadius: 3,
//         ),
//       ]),
//       child: AppBar(
//         title: title,
//         elevation: 0.0,
//         backgroundColor: Colors.transparent,
//       ),
//     );
//   }

//   @override
//   Size get preferredSize => Size.fromHeight(barHeight);
// }
