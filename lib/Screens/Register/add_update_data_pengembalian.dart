import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_application_2/modul/data_pengembalian.dart';
import 'package:flutter_application_2/screens/Register/list_data_pengembalian.dart';
import 'package:get/get.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/screens/Register/list_data_pengembalian.dart';
import 'package:flutter_application_2/utils/info.dart';


import '../../modul/data_pengembalian.dart';


class AddUpdatedata_pengembalian extends StatefulWidget {
  final Data_pengembalian? data_pengembalian;
  AddUpdatedata_pengembalian({this.data_pengembalian});

  @override
  State<AddUpdatedata_pengembalian> createState() => _AddUpdatedata_pengembalianState();
}

class _AddUpdatedata_pengembalianState extends State<AddUpdatedata_pengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkode_pengembalian = TextEditingController();
  var _controllerkode_pengajuan = TextEditingController();
  var _controllertanggal_pengajuan = TextEditingController();

  bool _isHidden = true;
  
  
  
  
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengembalian != null) {
      _controllerkode_pengembalian.text = widget.data_pengembalian!.kode_pengembalian!;
      _controllerkode_pengajuan.text = widget.data_pengembalian!.kode_pengajuan!;
      _controllertanggal_pengajuan.text = widget.data_pengembalian!.tanggal_kembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_pengembalian != null
            ? Text('Update Barang')
            : Text('Tambah Barang'),
        backgroundColor: kDefaultIconDarkColor,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_pengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_pengajuan,
                  decoration: InputDecoration(
                      labelText: "KOde Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggal_pengajuan,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengembalian == null) {
                        String message = await EventDb.Adddata_pengembalian(
                          _controllerkode_pengembalian.text,
                          _controllerkode_pengajuan.text,
                          _controllertanggal_pengajuan.text
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkode_pengembalian.clear();
                          _controllerkode_pengajuan.clear();
                          _controllertanggal_pengajuan.clear();
                          Get.off(
                            ListData_pengembalian(),
                          );
                        }
                      } else {
                        EventDb.Updatedata_pengembalian(
                          _controllerkode_pengembalian.text,
                          _controllerkode_pengajuan.text,
                          _controllertanggal_pengajuan.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: kDefaultIconDarkColor,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
