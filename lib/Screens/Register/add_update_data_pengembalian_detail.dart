import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_application_2/Screens/Register/list_data_pengembalian_detail.dart';
import 'package:get/get.dart';
import 'package:flutter_application_2/event/event_db.dart';
// import 'package:flutter_application_2/screens/Register/list_data_pengembalian_detail.dart';
import 'package:flutter_application_2/utils/info.dart';


import '../../modul/data_pengembalian_detail.dart';


class AddUpdateData_pengembalian_detail extends StatefulWidget {
  final Data_pengembalian_detail? data_pengembalian_detail;
  AddUpdateData_pengembalian_detail({this.data_pengembalian_detail});

  @override
  State<AddUpdateData_pengembalian_detail> createState() => _AddUpdateData_pengembalian_detailState();
}

class _AddUpdateData_pengembalian_detailState extends State<AddUpdateData_pengembalian_detail> {
  var _formKey = GlobalKey<FormState>();
  var _controllerNpm = TextEditingController();
  var _controllerNama = TextEditingController();
  var _controllerAlamat = TextEditingController();
  var _controllerFakultas = TextEditingController();
  var _controllerProdi = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengembalian_detail != null) {
      _controllerNpm.text = widget.data_pengembalian_detail!.mhsNpm!;
      _controllerNama.text = widget.data_pengembalian_detail!.mhsNama!;
      _controllerAlamat.text = widget.data_pengembalian_detail!.mhsAlamat!;
      _controllerFakultas.text = widget.data_pengembalian_detail!.mhsFakultas!;
      _controllerProdi.text = widget.data_pengembalian_detail!.mhsProdi!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_pengembalian_detail != null
            ? Text('Update Data_pengembalian_detail')
            : Text('Tambah Data_pengembalian_detail'),
        backgroundColor: kDefaultIconDarkColor,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengembalian_detail == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNpm,
                  decoration: InputDecoration(
                      labelText: "NPM",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerAlamat,
                  decoration: InputDecoration(
                      labelText: "Alamat",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerFakultas,
                  decoration: InputDecoration(
                      labelText: "Fakultas",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerProdi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengembalian_detail == null) {
                        String message = await EventDb.AddData_pengembalian_detail(
                          _controllerNpm.text,
                          _controllerNama.text,
                          _controllerAlamat.text,
                          _controllerFakultas.text,
                          _controllerProdi.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerNpm.clear();
                          _controllerNama.clear();
                          _controllerAlamat.clear();
                          _controllerFakultas.clear();
                          _controllerProdi.clear();
                          Get.off(
                            ListData_pengembalian_detail(),
                          );
                        }
                      } else {
                        EventDb.UpdateData_pengembalian_detail(
                          _controllerNpm.text,
                          _controllerNama.text,
                          _controllerAlamat.text,
                          _controllerFakultas.text,
                          _controllerProdi.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengembalian_detail == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: kDefaultIconDarkColor,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
