import 'package:flutter/material.dart';
import 'package:flutter_application_2/Screens/Register/RegisterComponent.dart';
import 'package:flutter_application_2/size_config.dart';

class RegisterScreens extends StatelessWidget{
static String routName = "/sign_up";

  @override
  Widget build(BuildContext context){
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
      ),
      body: RegisterComponent(),
    );
  }
}