import 'package:flutter/material.dart';
import 'package:flutter_application_2/utils/constants.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/Screens/Register/add_update_data_pengajuan.dart';

import '../../modul/data_pengajuan.dart';

class ListData_pengajuan extends StatefulWidget {
  @override
  State<ListData_pengajuan> createState() => _ListData_pengajuanState();
}

class _ListData_pengajuanState extends State<ListData_pengajuan> {
  List<Data_pengajuan> _listData_pengajuan = [];
  
  get data_pengajuan => null;

  void getData_pengajuan() async {
    _listData_pengajuan = await EventDb.getdata_pengajuan();

    setState(() {});
  }

  @override
  void initState() {
    getData_pengajuan();
    super.initState();
  }

  void showOption(Data_pengajuan? user) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatedata_pengajuan(data_pengajuan: data_pengajuan))?.then((value) => getData_pengajuan());
        break;
      // case 'delete':
      //   EventDb.deletedata_pengajuan(Data_pengajuan!.kode_pengajuan !).then((value) => getData_pengajuan());
      //   break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [kDefaultIconDarkColor, mBlueColor]),
        // titleSpacing: 0,
        title: Text('List Data_pengajuan'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listData_pengajuan.length > 0
              ? ListView.builder(
                  itemCount: _listData_pengajuan.length,
                  itemBuilder: (context, index) {
                    Data_pengajuan user = _listData_pengajuan[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(user.nama_peminjam ?? ''),
                      subtitle: Text(user.npm_peminjam ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(user),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatedata_pengajuan())?.then((value) => getData_pengajuan()),
              child: Icon(Icons.add),
              backgroundColor:kColorRedSlow,
            ),
          )
        ],
      ),
    );
  }
}

class GradientAppBar extends StatelessWidget implements PreferredSizeWidget {
  static const _defaultHeight = 56.0;

  final double elevation;
  final Gradient gradient;
  final Widget title;
  final double barHeight;

  GradientAppBar(
      {this.elevation = 3.0,
      required this.gradient,
      required this.title,
      this.barHeight = _defaultHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      decoration: BoxDecoration(gradient: gradient, boxShadow: [
        BoxShadow(
          offset: Offset(0, elevation),
          color: Colors.black.withOpacity(0.3),
          blurRadius: 3,
        ),
      ]),
      child: AppBar(
        title: title,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(barHeight);
}
