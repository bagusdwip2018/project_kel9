import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/modul/data_pengembalian.dart';
import 'package:flutter_application_2/screens/Register/add_update_data_pengembalian.dart';
// import 'package:project_kelas/screen/admin/add_update_mahasiswa.dart';

import '../../modul/data_pengembalian.dart';


class ListData_pengembalian extends StatefulWidget {
  @override
  State<ListData_pengembalian> createState() => _ListData_pengembalianState();
}

class _ListData_pengembalianState extends State<ListData_pengembalian> {
  List<Data_pengembalian> _listData_pengembalian = [];

  void getData_pengembalian() async {
    _listData_pengembalian = await EventDb.getData_pengembalian();

    setState(() {});
  }

  @override
  void initState() {
    getData_pengembalian();
    super.initState();
  }

  void showOption(Data_pengembalian? data_pengembalian) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      // case 'update':
      //   Get.to(AddUpdatedata_pengembalian(data_pengembalian: data_pengembalian))
      //       ?.then((value) => getData_pengembalian());
      //   break;
      case 'delete':
        EventDb.delete_data_pengembalian(data_pengembalian!.kode_pengembalian!)
            .then((value) => getData_pengembalian());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Data Barang'),
        backgroundColor: kDefaultIconDarkColor,
      ),
      body: Stack(
        children: [
          _listData_pengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listData_pengembalian.length,
                  itemBuilder: (context, index) {
                    Data_pengembalian data_pengembalian = _listData_pengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(data_pengembalian.kode_pengajuan ?? ''),
                      subtitle: Text(data_pengembalian.kode_pengembalian ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(data_pengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatedata_pengembalian())?.then((value) => getData_pengembalian()),
              child: Icon(Icons.add),
              backgroundColor: kDefaultIconDarkColor,
            ),
          )
        ],
      ),
    );
  }
}

AddUpdatedata_pengembalian() {
}
