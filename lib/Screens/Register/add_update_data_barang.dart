import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_application_2/modul/data_barang.dart';
import 'package:flutter_application_2/screens/Register/list_data_barang.dart';
import 'package:get/get.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/screens/Register/list_data_barang.dart';
import 'package:flutter_application_2/utils/info.dart';


import '../../modul/data_barang.dart';


class AddUpdatedata_barang extends StatefulWidget {
  final Data_barang? data_barang;
  AddUpdatedata_barang({this.data_barang});

  @override
  State<AddUpdatedata_barang> createState() => _AddUpdatedata_barangState();
}

class _AddUpdatedata_barangState extends State<AddUpdatedata_barang> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkode_barang = TextEditingController();
  var _controllernama_barang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_barang != null) {
      _controllerkode_barang.text = widget.data_barang!.kode_barang!;
      _controllernama_barang.text = widget.data_barang!.nama_barang!;
      _controllerjumlah.text = widget.data_barang!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_barang != null
            ? Text('Update Barang')
            : Text('Tambah Barang'),
        backgroundColor: kDefaultIconDarkColor,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_barang == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_barang,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernama_barang,
                  decoration: InputDecoration(
                      labelText: "Nama Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_barang == null) {
                        String message = await EventDb.Adddata_barang(
                          _controllerkode_barang.text,
                          _controllernama_barang.text,
                          _controllerjumlah.text
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkode_barang.clear();
                          _controllernama_barang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListData_barang(),
                          );
                        }
                      } else {
                        EventDb.Updatedata_barang(
                          _controllerkode_barang.text,
                          _controllernama_barang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_barang == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: kDefaultIconDarkColor,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
