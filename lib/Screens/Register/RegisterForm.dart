import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_application_2/Screens/Register/custom_surfix_icon.dart';
import 'package:flutter_application_2/Components/Login/LoginForm.dart';
import 'package:flutter_application_2/Screens/Login/LoginScreens.dart';
import 'package:flutter_application_2/Screens/Register/default_button_custome_color.dart';
import 'package:flutter_application_2/size_config.dart';
import 'package:flutter_application_2/utils/constants.dart';
import 'package:http/http.dart';

import '../../Api/configAPI.dart';


class SignUpform extends StatefulWidget {
  @override
  _SignUpform createState() => _SignUpform();
}

class _SignUpform extends State<SignUpform>{
  final _formkey = GlobalKey<FormState>();
  String? namalengkap;
  int? Npm;
  String? email;
  String? password;
  bool? remember = false;

  TextEditingController txtNamaLengkap = TextEditingController(),
  txtNpm = TextEditingController(),
  txtEmail = TextEditingController(),
  txtPassword = TextEditingController();

FocusNode focusNode = new FocusNode();

@override
void initState() {
  super.initState();
}

@override
Widget build(BuildContext context) {
  return Form(
    child: Column(
      children: [
        buildNamaLengkap(),
        SizedBox(height: getProportionateScreenHeight(30),),
        buildNpm(),
        SizedBox(height: getProportionateScreenHeight(30),),
        buildEmail(),
        SizedBox(height: getProportionateScreenHeight(30),),
        buildPassword(),
        SizedBox(height: getProportionateScreenHeight(30),),
        DefaultButtonCustomeColor(
          color: kPrimaryColor,
          text: 'Register',
          press: () {},
        ),
        SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, LoginScreens.routName);
          },
          child: Text(
            "Sudah punya akun? Masuk disini",
            style: TextStyle(decoration: TextDecoration.underline),
          ),
        )
      ],
    ),
  );
}
 TextFormField buildNamaLengkap() {
    return TextFormField(
      controller: txtNamaLengkap,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          hintText: 'Masukan nama lengkap anda',
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 117, 115, 107),
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.people,
                            color: Color.fromARGB(255, 117, 115, 107),
                          )),
                    );
                  }
    TextFormField buildNpm() {
    return TextFormField(
      controller: txtNpm,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          hintText: 'Masukan npm anda',
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 117, 115, 107),
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.people,
                            color: Color.fromARGB(255, 117, 115, 107),
                          )),
                    );
                  }
    TextFormField buildEmail() {
    return TextFormField(
      controller: txtEmail,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          hintText: 'Masukan email anda',
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 117, 115, 107),
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.people,
                            color: Color.fromARGB(255, 117, 115, 107),
                          )),
                    );
                  }
     TextFormField buildPassword() {
    return TextFormField(
      controller: txtPassword,
      obscureText: true,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          hintText: 'Masukan password anda',
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 117, 115, 107),
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Color.fromARGB(255, 117, 115, 107),
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBlueColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.vpn_key,
                            color: Color.fromARGB(255, 117, 115, 107),
                          )),
                    );
  }    
}