import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_application_2/modul/data_pengajuan.dart';
import 'package:flutter_application_2/utils/constants.dart';
import 'package:get/get.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/Screens/Register/list_data_pengajuan.dart';
import 'package:flutter_application_2/utils/info.dart';

import '../../modul/data_pengajuan.dart';

class AddUpdatedata_pengajuan extends StatefulWidget {
  final Data_pengajuan? data_pengajuan;
  AddUpdatedata_pengajuan({this.data_pengajuan});

  @override
  State<AddUpdatedata_pengajuan> createState() => _AddUpdatedata_pengajuanState();
}

class _AddUpdatedata_pengajuanState extends State<AddUpdatedata_pengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerKode_pengajuan = TextEditingController();
  var _controllerTanggal = TextEditingController();
  var _controllerNpm_peminjam = TextEditingController();
  var _controllerNama_peminjam = TextEditingController();
  var _controllerProdi = TextEditingController();
  var _controllerNomer_handphone = TextEditingController();

  List<String> role = [
    "Meja",
    "Kursi",
    "Proyektor",
    "Kelas",
  ];

  String _role = "Meja";

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengajuan != null) {
      _controllerKode_pengajuan.text = widget.data_pengajuan!.kode_pengajuan!;
      _controllerTanggal.text = widget.data_pengajuan!.tanggal!;
      _controllerNpm_peminjam.text = widget.data_pengajuan!.npm_peminjam!;
      _controllerNama_peminjam.text = widget.data_pengajuan!.nama_peminjam!;
      _controllerProdi.text = widget.data_pengajuan!.prodi!;
      _controllerNomer_handphone.text = widget.data_pengajuan!.nomer_handphone!;
      _role = widget.data_pengajuan!.kode_pengajuan!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [kDefaultIconDarkColor, kColorBlue]),
        // titleSpacing: 0,
        title: Text('List Data_pengajuan'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKode_pengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggal,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  enabled: widget.data_pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNpm_peminjam,
                  obscureText: _isHidden,
                  decoration: InputDecoration(
                      labelText: "Npm",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _isHidden = !_isHidden;
                          });
                        },
                        child: _isHidden
                            ? Icon(
                                Icons.visibility,
                                color: Colors.blue,
                              )
                            : Icon(
                                Icons.visibility_off,
                                color: Colors.red,
                              ),
                      )),
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField<String>(
                  value: _role,
                  decoration: InputDecoration(
                      labelText: "Role",
                      hintText: "Role",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  items: role.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _role = newValue!;
                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengajuan == null) {
                        String message = await EventDb.Add_data_pengajuan(
                          _controllerKode_pengajuan.text,
                          _controllerTanggal.text,
                          _controllerNpm_peminjam.text,
                          _controllerNama_peminjam.text,
                          _controllerProdi.text,
                          _controllerNomer_handphone.text,
                          
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerKode_pengajuan.clear();
                          _controllerTanggal.clear();
                          _controllerNama_peminjam.clear();
                          _controllerProdi.clear();
                          _controllerNomer_handphone.clear();
                          Get.off(
                            ListData_pengajuan(),
                          );
                        }
                      } else {
                        EventDb.Updatedata_pengajuan(
                            _controllerKode_pengajuan.text,
                            _controllerTanggal.text,
                            _controllerNama_peminjam.text,
                            _controllerProdi.text,
                            _controllerNomer_handphone.text,
                            _role);
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: kDefaultIconDarkColor,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
