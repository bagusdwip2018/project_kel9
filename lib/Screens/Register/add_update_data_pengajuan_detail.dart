// import 'package:flutter/material.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter/src/widgets/placeholder.dart';
// import 'package:flutter_application_2/modul/data_pengajuan_detail.dart';
// import 'package:flutter_application_2/utils/constants.dart';
// import 'package:get/get.dart';
// import 'package:flutter_application_2/event/event_db.dart';
// import 'package:flutter_application_2/Screens/Register/list_data_pengajuan_detail.dart';
// import 'package:flutter_application_2/utils/info.dart';

// import '../../modul/data_pengajuan_detail.dart';

// class AddUpdateData_pengajuan_detail extends StatefulWidget {
//   final Data_pengajuan_detail? data_pengajuan_detail;
//   AddUpdateData_pengajuan_detail({this.data_pengajuan_detail});

//   @override
//   State<AddUpdateData_pengajuan_detail> createState() => _AddUpdateData_pengajuan_detailState();
// }

// mixin Data_pengajuan_detail {
//   var kode_pengajuan;
  
//   var kode_barang;
  
//   var jumlah;
  
//   var role;
// }

// class _AddUpdateData_pengajuan_detailState extends State<AddUpdateData_pengajuan_detail> {
//   var _formKey = GlobalKey<FormState>();
//   var _controllerkode_pengajuan = TextEditingController();
//   var _controllerkode_barang = TextEditingController();
//   var _controllerjumlah = TextEditingController();

//   List<String> role = [
//     "Meja",
//     "Kursi",
//     "Proyektor",
//     "Ruangan",
//   ];

//   String _role = "Meja";

//   bool _isHidden = true;
//   @override
//   void initState() {
//     // TODO: implement initState
//     if (widget.data_pengajuan_detail != null) {
//       _controllerkode_pengajuan.text = widget.data_pengajuan_detail!.kode_pengajuan!;
//       _controllerkode_barang.text = widget.data_pengajuan_detail!.kode_barang!;
//       _controllerjumlah.text = widget.data_pengajuan_detail!.jumlah!;
//       _role = widget.data_pengajuan_detail!.role!;
//     }
//     super.initState();
//   }

//   Widget build(BuildContext context) {
//     return Scaffold(
//       resizeToAvoidBottomInset: false,
//       appBar: GradientAppBar(
//         gradient: LinearGradient(
//             colors: [kDefaultIconDarkColor, kColorBlue]),
//         // titleSpacing: 0,
//         title: Text('List Data_pengajuan_detail'),
//         // backgroundColor: Asset.colorPrimary,
//       ),
//       body: Stack(
//         children: [
//           Form(
//             key: _formKey,
//             child: ListView(
//               padding: EdgeInsets.all(16),
//               children: [
//                 TextFormField(
//                   validator: (value) => value == '' ? 'Jangan Kosong' : null,
//                   controller: _controllerkode_pengajuan,
//                   decoration: InputDecoration(
//                       labelText: "Kode Pengajuan",
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(10))),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 TextFormField(
//                   validator: (value) => value == '' ? 'Jangan Kosong' : null,
//                   controller: _controllerkode_barang,
//                   decoration: InputDecoration(
//                       labelText: "Kode Barang",
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(10))),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 TextFormField(
//                   enabled: widget.data_pengajuan_detail == null ? true : false,
//                   validator: (value) => value == '' ? 'Jangan Kosong' : null,
//                   controller: _controllerjumlah,
//                   obscureText: _isHidden,
//                   decoration: InputDecoration(
//                       labelText: "Jumlah",
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(10)),
//                       suffixIcon: GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             _isHidden = !_isHidden;
//                           });
//                         },
//                         child: _isHidden
//                             ? Icon(
//                                 Icons.visibility,
//                                 color: Colors.blue,
//                               )
//                             : Icon(
//                                 Icons.visibility_off,
//                                 color: Colors.red,
//                               ),
//                       )),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 DropdownButtonFormField<String>(
//                   value: _role,
//                   decoration: InputDecoration(
//                       labelText: "Role",
//                       hintText: "Role",
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(10))),
//                   items: role.map((String value) {
//                     return DropdownMenuItem<String>(
//                       value: value,
//                       child: Text(value),
//                     );
//                   }).toList(),
//                   onChanged: (String? newValue) {
//                     setState(() {
//                       _role = newValue!;
//                     });
//                   },
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 ElevatedButton(
//                   onPressed: () async {
//                     if (_formKey.currentState!.validate()) {
//                       if (widget.data_pengajuan_detail == null) {
//                         String message = await EventDb.addData_pengajuan_detail(
//                           _controllerkode_pengajuan.text,
//                           _controllerkode_barang.text,
//                           _controllerjumlah.text,
//                           _role,
//                         );
//                         Info.snackbar(message);
//                         if (message.contains('Berhasil')) {
//                           _controllerkode_pengajuan.clear();
//                           _controllerkode_barang.clear();
//                           _controllerjumlah.clear();
//                           Get.off(
//                             ListData_pengajuan_detail(),
//                           );
//                         }
//                       } else {
//                         EventDb.AddUpdateData_pengajuan_detail(
//                             _controllerkode_pengajuan.text,
//                             _controllerkode_barang.text,
//                             _controllerjumlah.text,
//                             _role);
//                       }
//                     }
//                   },
//                   child: Text(
//                     widget.data_pengajuan_detail == null ? 'Simpan' : 'Ubah',
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   style: ElevatedButton.styleFrom(
//                       primary: kDefaultIconDarkColor,
//                       fixedSize: Size.fromHeight(50),
//                       shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(5))),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
