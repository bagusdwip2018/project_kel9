import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/modul/data_pengembalian_detail.dart';
import 'package:flutter_application_2/screens/Register/add_update_data_pengembalian_detail.dart';
// import 'package:project_kelas/screen/admin/add_update_data_pengembalian_detail.dart';

import '../../modul/data_pengembalian_detail.dart';


class ListData_pengembalian_detail extends StatefulWidget {
  @override
  State<ListData_pengembalian_detail> createState() => _ListData_pengembalian_detailState();
}

class _ListData_pengembalian_detailState extends State<ListData_pengembalian_detail> {
  List<Data_pengembalian_detail> _listData_pengembalian_detail = [];

  void getData_pengembalian_detail() async {
    _listData_pengembalian_detail = await EventDb.getData_pengembalian_detail();

    setState(() {});
  }

  @override
  void initState() {
    getData_pengembalian_detail();
    super.initState();
  }

  void showOption(Data_pengembalian_detail? data_pengembalian_detail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdateData_pengembalian_detail(data_pengembalian_detail: data_pengembalian_detail))
            ?.then((value) => getData_pengembalian_detail());
        break;
      case 'delete':
        EventDb.deleteData_pengembalian_detail(data_pengembalian_detail!.mhsNpm!)
            .then((value) => getData_pengembalian_detail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Data_pengembalian_detail'),
        backgroundColor: kDefaultIconDarkColor,
      ),
      body: Stack(
        children: [
          _listData_pengembalian_detail.length > 0
              ? ListView.builder(
                  itemCount: _listData_pengembalian_detail.length,
                  itemBuilder: (context, index) {
                    Data_pengembalian_detail data_pengembalian_detail = _listData_pengembalian_detail[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(data_pengembalian_detail.mhsNama ?? ''),
                      subtitle: Text(data_pengembalian_detail.mhsNpm ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(data_pengembalian_detail),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdateData_pengembalian_detail())?.then((value) => getData_pengembalian_detail()),
              child: Icon(Icons.add),
              backgroundColor: kDefaultIconDarkColor,
            ),
          )
        ],
      ),
    );
  }
}
