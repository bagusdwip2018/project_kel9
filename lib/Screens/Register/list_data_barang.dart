import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:flutter_application_2/event/event_db.dart';
import 'package:flutter_application_2/modul/data_barang.dart';
import 'package:flutter_application_2/screens/Register/add_update_data_barang.dart';
// import 'package:project_kelas/screen/admin/add_update_mahasiswa.dart';

import '../../modul/data_barang.dart';


class ListData_barang extends StatefulWidget {
  @override
  State<ListData_barang> createState() => _ListData_barangState();
}

class _ListData_barangState extends State<ListData_barang> {
  List<Data_barang> _listData_barang = [];

  void getData_barang() async {
    _listData_barang = await EventDb.getData_barang();

    setState(() {});
  }

  @override
  void initState() {
    getData_barang();
    super.initState();
  }

  void showOption(Data_barang? data_barang) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatedata_barang(data_barang: data_barang))
            ?.then((value) => getData_barang());
        break;
      case 'delete':
        EventDb.delete_data_barang(data_barang!.kode_barang!)
            .then((value) => getData_barang());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Data Barang'),
        backgroundColor: kDefaultIconDarkColor,
      ),
      body: Stack(
        children: [
          _listData_barang.length > 0
              ? ListView.builder(
                  itemCount: _listData_barang.length,
                  itemBuilder: (context, index) {
                    Data_barang data_barang = _listData_barang[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(data_barang.kode_barang ?? ''),
                      subtitle: Text(data_barang.nama_barang ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(data_barang),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatedata_barang())?.then((value) => getData_barang()),
              child: Icon(Icons.add),
              backgroundColor: kDefaultIconDarkColor,
            ),
          )
        ],
      ),
    );
  }
}
