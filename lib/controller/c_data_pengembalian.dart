import 'package:get/get.dart';
import 'package:flutter_application_2/modul/data_pengembalian.dart';

class CData_pengembalian extends GetxController {
  Rx<Data_pengembalian> _data_pengembalian = Data_pengembalian().obs;

  Data_pengembalian get user => _data_pengembalian.value;

  void setUser(Data_pengembalian data_pengembalian) => _data_pengembalian.value = data_pengembalian;
}
