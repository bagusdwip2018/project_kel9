import 'package:get/get.dart';
import 'package:flutter_application_2/modul/data_barang.dart';

class Cdata_barang extends GetxController {
  Rx<Data_barang> _data_barang= Data_barang().obs;

  Data_barang get data_barang => _data_barang.value;

  void setData_barang(Data_barang data_barang) => _data_barang.value = data_barang;
}
