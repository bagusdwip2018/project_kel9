import 'package:get/get.dart';
import 'package:flutter_application_2/modul/data_pengajuan_detail.dart';

class CData_pengajuan_detail extends GetxController {
  Rx<Data_pengajuan_detail> _listdata_pengajuan_detail= Data_pengajuan_detail().obs;

  Data_pengajuan_detail get user => _data_pengajuan_detail.value;

  void setUser(Data_pengajuan_detail data_pengajuan_detail) => _data_pengajuan_detail.value = data_pengajuan_detail;
}

mixin _data_pengajuan_detail {
  static var value;
}