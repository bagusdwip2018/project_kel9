import 'package:get/get.dart';
import 'package:flutter_application_2/modul/data_pengembalian_detail.dart';

class CData_pengembalian_detail extends GetxController {
  Rx<Data_pengembalian_detail> _data_pengembalian_detail = Data_pengembalian_detail().obs;

  Data_pengembalian_detail get user => _data_pengembalian_detail.value;

  void setUser(Data_pengembalian_detail dataData_pengembalian_detail) => _data_pengembalian_detail.value = dataData_pengembalian_detail;
}