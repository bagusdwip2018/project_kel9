import 'package:get/get.dart';
import 'package:flutter_application_2/modul/data_pengajuan.dart';

class Cdata_pengajuan extends GetxController {
  Rx<Data_pengajuan> _data_pengajuan= Data_pengajuan().obs;

  Data_pengajuan get data_pengajuan => _data_pengajuan.value;

  void setData_pengajuan(Data_pengajuan data_pengajuan) => _data_pengajuan.value = Data_pengajuan();
}
