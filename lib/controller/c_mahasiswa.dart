import 'package:get/get.dart';
import 'package:flutter_application_2/modul/mahasiswa.dart';

class CMahasiswa extends GetxController {
  Rx<Mahasiswa> _mahasiswa = Mahasiswa().obs;

  Mahasiswa get user => _mahasiswa.value;

  void setUser(Mahasiswa dataMahasiswa) => _mahasiswa.value = dataMahasiswa;
}
