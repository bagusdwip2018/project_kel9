import 'dart:convert';
import 'package:flutter_application_2/Screens/Register/add_update_data_pengajuan.dart';
import 'package:flutter_application_2/Screens/Register/list_data_pengajuan.dart';
import 'package:flutter_application_2/modul/data_barang.dart';
import 'package:flutter_application_2/modul/data_pengajuan.dart';
import 'package:flutter_application_2/modul/data_pengajuan_detail.dart';
import 'package:flutter_application_2/modul/data_pengembalian.dart';
import 'package:flutter_application_2/modul/data_pengembalian_detail.dart';
import 'package:get/get.dart';
import 'package:flutter_application_2/event/event_pref.dart';
import 'package:flutter_application_2/modul/mahasiswa.dart';
import 'package:flutter_application_2/modul/user.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_application_2/Components/login/LoginForm.dart';
import 'package:flutter_application_2/Api/configAPI.dart';
import 'package:flutter_application_2/Components/Login/LoginForm.dart';


import '../utils/info.dart';


class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              login(username, pass),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Mahasiswa>> getMahasiswa() async {
    List<Mahasiswa> listMahasiswa = [];

    try {
      var response = await http.get(Uri.parse(Api.getMahasiswa));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var mahasiswa = responBody['mahasiswa'];

          mahasiswa.forEach((mahasiswa) {
            listMahasiswa.add(Mahasiswa.fromJson(mahasiswa));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listMahasiswa;
  }

  static Future<String> AddMahasiswa(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addMahasiswa), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Mahasiswa Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateMahasiswa(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    try {
      var response = await http.post(Uri.parse(Api.updateMahasiswa), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Mahasiswa');
        } else {
          Info.snackbar('Gagal Update Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteMahasiswa(String mhsNpm) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteMahasiswa), body: {'mhsNpm': mhsNpm});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Mahasiswa');
        } else {
          Info.snackbar('Gagal Delete Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //data_barang =====================================================================================
  static Future<List<Data_barang>> getData_barang() async {
    List<Data_barang> listData_barang = [];

    try {
      var response = await http.get(Uri.parse(Api.getdata_barang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var data_barang= responBody['data barang'];

          data_barang.forEach((data_barang) {
            listData_barang.add(Data_barang.fromJson(data_barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listData_barang;
  }

  static Future<String> Adddata_barang(String kode_barang, String nama_barang,
      String jumlah,) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.adddata_barang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> Updatedata_barang(String kode_barang, String nama_barang,
      String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updatedata_barang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Barang');
        } else {
          Info.snackbar('Gagal Update Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> delete_data_barang(String kode_barang) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletedata_barang), body: {'kode_barang': kode_barang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Barang');
        } else {
          Info.snackbar('Gagal Delete Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  // Data pengajuan=================================================================================
  static Future<List<Data_pengajuan>> getdata_pengajuan() async {
    List<Data_pengajuan> listData_pengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getdata_pengajuan));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var data_pengajuan= responBody['data pengajuan'];

          data_pengajuan.forEach((data_pengajuan) {
            listData_pengajuan.add(Data_pengajuan.fromJson(data_pengajuan));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listData_pengajuan;
  }

  static Future<String> Add_data_pengajuan(String kode_pengajuan, String tanggal,
      String npm_peminjam, String nama_peminjam, String prodi,String nomer_handphone) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.adddata_pengajuan), body: {
        'kode_pengajuan': kode_pengajuan,
        'tanggal': tanggal,
        'npm_peminjam': npm_peminjam,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
        'nomer_handphone': nomer_handphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> Updatedata_pengajuan(String kode_pengajuan, String tanggal,
      String npm_peminjam, String nama_peminjam, String prodi,String nomer_handphone) async {
    try {
      var response = await http.post(Uri.parse(Api.updatedata_pengajuan), body: {
        'kode_pengajuan': kode_pengajuan,
        'tanggal': tanggal,
        'npm_peminjam': npm_peminjam,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
        'nomer_handphone': nomer_handphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengajuan');
        } else {
          Info.snackbar('Gagal Update Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> delete_data_pengajuan(String kode_pengajuan) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletedata_pengajuan), body: {'kode_pengajuan': kode_pengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengajuan');
        } else {
          Info.snackbar('Gagal Delete Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  // Data Pengajuan_detail============================================================================================================
  static Future<List<Data_pengajuan_detail>> getData_pengajuan_detail() async {
    List<Data_pengajuan_detail> listData_pengajuan_detail = [];

    try {
      var response = await http.get(Uri.parse(Api.getdata_pengajuan_detail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var data_pengajuan_detail= responBody['data pengajuan detail'];

          data_pengajuan_detail.forEach((data_pengajuan_detail) {
            listData_pengajuan_detail.add(Data_pengajuan_detail.fromJson(data_pengajuan_detail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listData_pengajuan_detail;
  }

  static Future<String> Adddata_pengajuan_detail(String kode_pengajuan, String kode_barang,
      String jumlah,) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.adddata_barang), body: {
        'kode_pengajuan': kode_pengajuan,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> Updatedata_pengajuan_detail(String kode_pengajuan, String kode_barang,
      String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updatedata_pengajuan_detail), body: {
        'kode_pengajuan': kode_pengajuan,
        'kode_barang': kode_barang,
        'jumlah': jumlah
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengajuan');
        } else {
          Info.snackbar('Gagal Update Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> delete_data_pengajuan_detail(String kode_pengajuan) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletedata_barang), body: {'kode_pengajuan': kode_pengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengajuan');
        } else {
          Info.snackbar('Gagal Delete Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Data Pengembalian=====================================================================================================
  static Future<List<Data_pengembalian>> getData_pengembalian() async {
    List<Data_pengembalian> listData_pengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getdata_pengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var data_pengembalian= responBody['data pengembalian'];

          data_pengembalian.forEach((data_pengembalian) {
            listData_pengembalian.add(Data_pengembalian.fromJson(data_pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listData_pengembalian;
  }

  static Future<String> Adddata_pengembalian(String kode_pengembalian, String kode_pengajuan,
      String tanggal_kembali,) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.adddata_pengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan': kode_pengajuan,
        'tanggal_kembali': tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> Updatedata_pengembalian(String kode_pengembalian, String kode_pengajuan,
      String tanggal_kembali) async {
    try {
      var response = await http.post(Uri.parse(Api.updatedata_barang), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan': kode_pengajuan,
        'tanggal_kembali': tanggal_kembali
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian');
        } else {
          Info.snackbar('Gagal Update Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> delete_data_pengembalian (String kode_pengembalian) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletedata_pengembalian), body: {'kode_pengembalian': kode_pengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Data Pengembalian Detail==========================================================================================================
  static Future<List<Data_pengembalian_detail>> getData_pengembalian_detail() async {
    List<Data_pengembalian_detail> listData_pengembalian_detail = [];

    try {
      var response = await http.get(Uri.parse(Api.getdata_pengembalian_detail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var data_pengembalian_detail= responBody['data pengembalian_detail'];

          data_pengembalian_detail.forEach((data_pengembalian_detail) {
            listData_pengembalian_detail.add(Data_pengembalian_detail.fromJson(data_pengembalian_detail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listData_pengembalian_detail;
  }

  static Future<String> AddData_pengembalian_detail(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.adddata_pengembalian_detail), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateData_pengembalian_detail (String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    try {
      var response = await http.post(Uri.parse(Api.updatedata_pengembalian_detail), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian');
        } else {
          Info.snackbar('Gagal Update Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteData_pengembalian_detail (String mhsNpm) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteMahasiswa), body: {'mhsNpm': mhsNpm});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

 

  // static getData_pengajuan() {}

  // static deletedata_pengajuan(param0) {}

  // static adddata_pengajuan(String text, String text2, String text3, String text4, String text5, String text6, String role) {}

  // static void updatedata_pengajuan(String text, String text2, String text3, String text4, String text5, String role) {}

  // // static add_data_barang(String text, String text2, String text3) {}

  // // static void updatedata_barang(String text, String text2, String text3) {}


  // static addData_pengajuan_detail(text, text2, text3, String role) {}

  // static void AddUpdateData_pengajuan_detail(String text, String text2, String text3, String role) {}

  // static getData_pengajuan_detail() {}

  // static delete_data_pengembalian(String s) {}

  // static void updatedata_pengembalian(String text, String text2, String text3) {}

  // static addData_pengembalian(String text, String text2, String text3) {}

  // static getData_pengembalian() {}

  // static deleteData_pengembalian_detail(String s) {}

  // static getData_pengembalian_detail() {}

  // static AddData_pengembalian_detail(String text, String text2, String text3, String text4, String text5) {}

  // static void UpdateData_pengembalian_detail(String text, String text2, String text3, String text4, String text5) {}
}
