class Api {
  static const _host = "http://192.168.1.8:8080/api_kel";

  static String _user = "$_host/user";
  static String _mahasiswa = "$_host/mahasiswa";
  static String _data_barang = "$_host/data_barang";
  static String _data_pengajuan = "$_host/data_pengajuan";

  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  // mahasiswa
  static String addMahasiswa = "$_mahasiswa/add_mahasiswa.php";
  static String deleteMahasiswa = "$_mahasiswa/delete_mahasiswa.php";
  static String getMahasiswa = "$_mahasiswa/get_mahasiswa.php";
  static String updateMahasiswa = "$_mahasiswa/update_mahasiswa.php";

   // data_barang
  static String adddata_barang = "$_data_barang/add_data_barang.php";
  static String deletedata_barang = "$_data_barang/delete_data_barang.php";
  static String getdata_barang = "$_data_barang/get_data_barang.php";
  static String updatedata_barang = "$_data_barang/update_data_barang.php";

  // data_pengajuan
  static String adddata_pengajuan = "$_user/add_data_pengajuan.php";
  static String deletedata_pengajuan = "$_user/delete_data_pengajuan.php";
  static String getdata_pengajuan = "$_user/get_data_pengajuan.php";
  static String updatedata_pengajuan = "$_user/update_data_pengajuan.php";

   // data_pengajuan_detail
  static String adddata_pengajuan_detail = "$_user/add_data_pengajuan_detail.php";
  static String deletedata_pengajuan_detail = "$_user/delete_data_pengajuan_detail.php";
  static String getdata_pengajuan_detail = "$_user/get_data_pengajuan_detail.php";
  static String updatedata_pengajuan_detail = "$_user/update_data_pengajuan_detail.php";

  // data_pengembalian
  static String adddata_pengembalian = "$_user/add_data_pengembalian.php";
  static String deletedata_pengembalian = "$_user/delete_data_pengembalian.php";
  static String getdata_pengembalian = "$_user/get_data_pengembalian.php";
  static String updatedata_pengembalian = "$_user/update_data_pengembalian.php";

    // data_pengembalian_detail
  static String adddata_pengembalian_detail = "$_user/add_data_pengembalian_detail.php";
  static String deletedata_pengembalian_detail = "$_user/delete_data_pengembalian_detail.php";
  static String getdata_pengembalian_detail = "$_user/get_data_pengembalian_detail.php";
  static String updatedata_pengembalian_detail = "$_user/update_data_pengembalian_detail.php";
}


