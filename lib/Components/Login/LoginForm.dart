import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_application_2/Screens/Register/RegistrasiScreens.dart';
import 'package:flutter_application_2/Screens/Register/custom_surfix_icon.dart';
import 'package:flutter_application_2/Screens/Register/default_button_custome_color.dart';
import 'package:flutter_application_2/utils/constants.dart';

import '../../home/Home.dart';

class SignInform extends StatefulWidget {
  @override
  _SignInform createState() => _SignInform();
}

class _SignInform extends State<SignInform> {
  final _formkey = GlobalKey<FormState>();
  String? npm;
  String? password;
  bool? remeber = false;

  TextEditingController txtNpm = TextEditingController(),
      txtPassword = TextEditingController();

  FocusNode focusNode = new FocusNode();
  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(children: [
        buildNpm(),
        SizedBox(
          height: 14,
        ),
        buildPassword(),
        SizedBox(
          height: 14,
        ),
        Row(
          children: [ 
              Checkbox(
                value: remeber,
                onChanged: (value) {
                  setState(() {
                    remeber = value;
                  });
                }),
            Text("Tetap Masuk"),
            Spacer(),
            GestureDetector(
              onTap: () {},
              child: Text(
                "Lupa Password",
                style: TextStyle(decoration: TextDecoration.underline),
              ),
            )
          ],
        ),
        DefaultButtonCustomeColor(
          color: kColorRedSlow,
          text:"Masuk",
          press: () {
          },
        ),
        SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, RegisterScreens.routName);
          },
          child: Text(
            "Belum Punya Akun ? Daftar Disini",
            style: TextStyle(decoration: TextDecoration.underline),
          ),
        ),
         SizedBox(
            height: 8,
             ),
          Container(
                        margin: EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          'developed by kelompok n',
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                          ),
                        ),
                      ),
      ]),
    );
  }

  TextFormField buildNpm() {
    return TextFormField(
      controller: txtNpm,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          hintText: 'Masukan npm anda',
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 117, 115, 107),
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.people,
                            color: Color.fromARGB(255, 117, 115, 107),
                          )),
                    );
  }
  TextFormField buildPassword() {
    return TextFormField(
      controller: txtPassword,
      obscureText: true,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          hintText: 'Masukan password anda',
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 117, 115, 107),
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Color.fromARGB(255, 117, 115, 107),
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBlueColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: mBorderColor,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.vpn_key,
                            color: Color.fromARGB(255, 117, 115, 107),
                          )),
                    );
  }
}
