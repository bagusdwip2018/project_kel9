import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_2/Components/Login/LoginForm.dart';
import 'package:flutter_application_2/size_config.dart';
import 'package:flutter_application_2/utils/constants.dart';

class LoginComponent extends StatefulWidget {
  @override
  // ignore: library_private_types_in_public_api
  _LoginComponent createState() => _LoginComponent();
}

class _LoginComponent extends State<LoginComponent> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenHeight(20)),
          child: SingleChildScrollView(
            child: Column(children: [
              SizedBox(
                height: SizeConfig.screenHeight * 0.04,
              ),
              SizedBox(
                height: SizeConfig.screenHeight * 0.04,
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: 150,
                height: 150,
                padding: EdgeInsets.all(3),
                child: Image(
                  image: NetworkImage(
                      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/UNIVERSITASTEKNOKRAT.png/1200px-UNIVERSITASTEKNOKRAT.png"),
                  fit: BoxFit.contain,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Login !", 
                    style: mTitleStyle,
                    )
                  ],
                ),
              ),
              SizedBox(height: 20,
              ),
              SignInform()
            ]),
          ),
        ),
      ),
    );
  }
}
